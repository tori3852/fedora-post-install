# .bashrc

export EDITOR=vim
export VISUAL=$EDITOR

export JAVA_HOME=~/bin/java/default
export M2_HOME=~/bin/maven/default

export PATH=$JAVA_HOME/bin:$M2_HOME/bin:$PATH

alias mvn="mvn -V"

# zsh config
prompt steeef # theme

# for cron+notify-send
env | grep DBUS_SESSION_BUS_ADDRESS | tee $HOME/.dbus/Xdbus > /dev/null

