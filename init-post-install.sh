
# RPMFusion

wget http://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm
wget http://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm
dnf install -y rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm
# TODO: remove rpms

dnf update -y

# Tools
dnf install -y \
  vlc \
  git gitg \
  subversion \
  mc \
  vim-enhanced \
  htop iotop iftop \
  gimp \
  nmap \
  wget \
  tmux \
  terminator \
  gparted \
  lshw \
  keepass \
  youtube-dl \
  lm_sensors hddtemp \
  meld \
  wireshark \
  speedtest-cli \
  zsh \
  gnome-tweak-tool \
  nautilus-dropbox \
  gtimelog \
  ImageMagick \
  jq \
  graphviz

# Git config
git config --global push.default simple
touch ~/.gitignore_global && git config --global core.excludesfile ~/.gitignore_global

# ZSH
dnf install util-linux-user # todo: needed for chsh from f24
chsh $USER -s /bin/zsh
zsh -c 'git clone --recursive https://github.com/sorin-ionescu/prezto.git "${ZDOTDIR:-$HOME}/.zprezto"'
zsh -c 'setopt EXTENDED_GLOB; for rcfile in "${ZDOTDIR:-$HOME}"/.zprezto/runcoms/^README.md(.N); do ln -s "$rcfile" "${ZDOTDIR:-$HOME}/.${rcfile:t}"; done'

dnf groupinstall -y "Development tools"

# VirtualBox
curl http://download.virtualbox.org/virtualbox/rpm/fedora/virtualbox.repo | tee /etc/yum.repos.d/virtualbox.repo

dnf update -y

dnf install -y \
  VirtualBox-5.0
dnf install -y \
  kernel-devel

/sbin/rcvboxdrv setup

usermod -a -G vboxusers $USER

# Make Python3 default
alternatives --install /usr/bin/python python /usr/bin/python3.5 2
alternatives --install /usr/bin/python python /usr/bin/python2.7 1
alternatives --list | grep python

pip3 install \
  git-up \
  git-sweep \
  argcomplete \
  rainbow configparser

# Fonts
dnf install -y \
  levien-inconsolata-fonts \
  adobe-source-code-pro-fonts

wget https://github.com/tonsky/FiraCode/raw/master/distr/otf/FiraCode-Bold.otf -P ~/.fonts/
wget https://github.com/tonsky/FiraCode/raw/master/distr/otf/FiraCode-Light.otf -P ~/.fonts/
wget https://github.com/tonsky/FiraCode/raw/master/distr/otf/FiraCode-Medium.otf -P ~/.fonts/
wget https://github.com/tonsky/FiraCode/raw/master/distr/otf/FiraCode-Regular.otf -P ~/.fonts/

# Gnome extensions
git clone https://github.com/iacopodeenosee/EasyScreenCast ~/.local/share/gnome-shell/extensions/EasyScreenCast@iacopodeenosee.gmail.com

gsettings list-recursively > .gsettings.bak.$(date +"%s")

gsettings set org.gnome.shell.overrides workspaces-only-on-primary false
gsettings set org.gnome.desktop.datetime automatic-timezone true
gsettings set org.gnome.desktop.interface clock-show-date true
gsettings set org.gnome.desktop.interface clock-show-seconds true
gsettings set org.gnome.desktop.interface monospace-font-name 'Inconsolata Medium 16'
# Deprecated, for Terminator
gconftool-2 --set /desktop/gnome/interface/monospace_font_name --type string "Inconsolata Medium 16"
gsettings set org.gnome.shell enabled-extensions "['alternate-tab@gnome-shell-extensions.gcampax.github.com', 'EasyScreenCast@iacopodeenosee.gmail.com']"
gsettings set org.gnome.desktop.input-sources sources "[('xkb', 'lt+us'), ('xkb', 'ru+phonetic')]"
gsettings set org.gnome.desktop.wm.keybindings begin-resize "[]"
gsettings set org.gnome.desktop.wm.keybindings begin-move "[]"

gsettings set org.gnome.desktop.peripherals.touchpad tap-to-click true
gsettings set org.gnome.desktop.media-handling automount false

gsettings set org.gnome.system.location enabled false
gsettings set org.gnome.desktop.privacy report-technical-problems false

gsettings set org.gnome.shell.overrides workspaces-only-on-primary false

# Whey typing in Nautilus - don't do recursive search
gsettings set org.gnome.nautilus.preferences recursive-search 'never'

gsettings set org.gnome.nautilus.preferences sort-directories-first true

gsettings set org.gnome.desktop.background show-desktop-icons true

